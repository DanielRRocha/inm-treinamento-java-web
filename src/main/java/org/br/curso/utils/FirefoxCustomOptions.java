package org.br.curso.utils;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.firefox.FirefoxOptions;

import br.com.santander.frm.interfaces.CustomDriverOptions;

public class FirefoxCustomOptions  implements CustomDriverOptions {

	public <T extends MutableCapabilities> T getOptions() {
		
		FirefoxOptions xpto = new FirefoxOptions();
		xpto.setHeadless(false);
		
		return (T)xpto;
	}
	
}
