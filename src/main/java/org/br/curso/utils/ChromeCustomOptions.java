package org.br.curso.utils;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.chrome.ChromeOptions;

import br.com.santander.frm.interfaces.CustomDriverOptions;

public class ChromeCustomOptions  implements CustomDriverOptions {

	public <T extends MutableCapabilities> T getOptions() {		
		
		ChromeOptions xpto = new ChromeOptions();
		xpto.setHeadless(false);
	
		return (T)xpto;
	}

}
